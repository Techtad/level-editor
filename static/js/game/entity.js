class Entity {
    constructor() {
        this.container = new THREE.Object3D()

        let boxGeom = new THREE.BoxGeometry(25, 25, 25)
        this.model = new THREE.Mesh(boxGeom, Materials.GridBlue) //TODO: Zmienić na właściwy model
        this.model.position.y += 12.5
        this.container.add(this.model)

        this.axes = new THREE.AxesHelper(50)
        this.axes.position.y += 12.5
        this.container.add(this.axes)

        this.destination = new THREE.Vector3(0, 0, 0)
        this.direction = new THREE.Vector3(0, 0, 0)
        this.moving = false
        this.speed = Settings.DEFAULT_SPEED
    }

    getContainer() {
        return this.container
    }

    getModel() {
        return this.model
    }

    getAxes() {
        return this.axes
    }

    setSpeed(s) {
        this.speed = s
    }

    moveTo(vector) {
        let angle = Math.atan2(
            this.container.position.x - vector.x,
            this.container.position.z - vector.z
        ) + Math.PI
        this.model.rotation.y = angle
        this.axes.rotation.y = angle

        this.destination = vector
        this.direction = vector.clone().sub(this.container.position).normalize()
        this.moving = true
    }

    update(delta) {
        if (this.moving) {
            let distance = this.container.position.distanceTo(this.destination)
            if (distance > 0.05) {
                let motion = Math.min(distance, this.speed * delta)
                this.container.translateOnAxis(this.direction, motion)
            } else {
                this.moving = false
            }
        }
    }
}