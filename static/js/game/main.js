var Game = {
    init: function () {
        this.scene = new THREE.Scene()

        this.renderer = new THREE.WebGLRenderer({ antialias: true })
        this.renderer.setClearColor(0xffffff)
        this.renderer.setSize($(window).width(), $(window).height())

        this.camera = new THREE.PerspectiveCamera(45, $(window).width() / $(window).height(), 1, 10000)
        this.camera.position.set(Settings.INIT_CAM_POS.X, Settings.INIT_CAM_POS.Y, Settings.INIT_CAM_POS.Z)
        this.camera.lookAt(this.scene.position)
        this.camera.updateProjectionMatrix()

        window.addEventListener("resize", (e) => { Game.renderer.setSize($(window).width(), $(window).height()); Game.camera.aspect = $(window).width() / $(window).height(); Game.camera.updateProjectionMatrix(); })

        this.orbitControls = new THREE.OrbitControls(this.camera, this.renderer.domElement)
        this.orbitControls.enabled = false

        this.clock = new THREE.Clock()

        $("#root").append(this.renderer.domElement)
        this.createScene()
    },

    createScene: function () {
        this.baseplate = new BasePlate(2000, 2000)
        this.scene.add(this.baseplate.getMesh())
        this.sun = new THREE.DirectionalLight(0xffffff, 0.25)
        this.scene.add(this.sun)

        this.floors = []
        this.walls = []
        LevelLoader.loadFromServer(PageUtils.getURLParam("lvl")).then((level) => {
            this.level = level
            for (let hex of level.getMap()) {
                this.scene.add(hex.getContainer())
                this.floors.push(hex.floor)
                this.walls = this.walls.concat(hex.walls)
            }
        })

        this.player = new Player()
        this.scene.add(this.player.getContainer())

        this.collIndicator = new THREE.Mesh(new THREE.SphereGeometry(10, 16, 16), Materials.GridGreen)
        this.scene.add(this.collIndicator)
        this.player.collIndicator = this.collIndicator

        this.allyObjects = []
        this.availableAllies = new THREE.Object3D()
        this.scene.add(this.availableAllies)
        this.highlightedAlly = null

        this.ui = new UI()
    },

    update: function () {
        let delta = this.clock.getDelta() / 0.016
        this.player.update(delta)
        for (let ally of this.allyObjects) { ally.update(delta) }

        this.camera.position.x = this.player.getContainer().position.x
        this.camera.position.z = this.player.getContainer().position.z + 400
        this.camera.position.y = this.player.getContainer().position.y + 400
        this.camera.lookAt(this.player.getContainer().position)

        this.render()
    },

    render: function () {
        this.renderer.render(this.scene, this.camera)
        requestAnimationFrame(this.update.bind(this))
    },


    start: function () {
        requestAnimationFrame(this.update.bind(this))
    }
}

$(document).ready(function (event) {
    Game.init()
    Game.start()
})