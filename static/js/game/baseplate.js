class BasePlate {
    constructor(width, depth) {
        this.geometry = new THREE.PlaneGeometry(width, depth, width / Settings.HEX_RADIUS / 2, depth / Settings.HEX_RADIUS / Math.sqrt(3))
        this.mesh = new THREE.Mesh(this.geometry, Materials.BasePlate)
        this.mesh.rotation.set(Math.PI / 2, 0, 0)
    }

    getMesh() {
        return this.mesh
    }
}