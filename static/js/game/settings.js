var Settings = {
    INIT_CAM_POS: {
        X: 0,
        Y: 1200,
        Z: 0
    },

    HEX_RADIUS: 200,
    WALL_WIDTH: 10,
    WALL_HEIGHT: 100,
    WALL_LENGTH: function () { return this.HEX_RADIUS },

    LIGHT_INTENSITY: 1,
    LIGHT_DISTANCE: 400,
    LIGHT_HEIGHT: 50,

    TREASURE_SIZE: 75,

    DEFAULT_SPEED: 2,
    PLAYER_SPEED: 2,
    ALLY_SPEED: 2,

    ALLY_DISTANCE: 50
}

var Materials = {
    BasePlate: new THREE.MeshBasicMaterial({
        wireframe: true,
        color: 0x000000
    }),

    Wall: new THREE.MeshPhongMaterial({
        color: 0xaaaaaa
    }),

    Floor: new THREE.MeshPhongMaterial({
        color: 0xaaaaaa,
        side: THREE.DoubleSide
    }),

    LightIndicator: new THREE.MeshBasicMaterial({
        wireframe: true,
        color: 0xffff00
    }),

    Treasure: new THREE.MeshPhongMaterial({
        color: 0xff0000
    }),

    Grid: new THREE.MeshBasicMaterial({
        color: 0,
        wireframe: true
    }),

    GridRed: new THREE.MeshBasicMaterial({
        color: 0xff0000,
        wireframe: true
    }),

    GridGreen: new THREE.MeshBasicMaterial({
        color: 0x00ff00,
        wireframe: true
    }),

    GridBlue: new THREE.MeshBasicMaterial({
        color: 0x0000ff,
        wireframe: true
    })
}

var TextureLoader = new THREE.TextureLoader()
var ModelLoader = new THREE.JSONLoader()