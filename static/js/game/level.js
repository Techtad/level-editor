class Level {
    constructor(size, level) {
        this.size = size
        this.level = level

        this.map = []
        this.lights = []
        this.items = []

        let minX = 0, minZ = 0, maxX = 0, maxZ = 0
        for (let hex of level) {
            if (hex.x < minX) minX = hex.x
            if (hex.x > maxX) maxX = hex.x
            if (hex.z < minZ) minZ = hex.z
            if (hex.z > maxZ) maxZ = hex.z
        }

        for (let hex of level) {
            let x = hex.x * (Settings.HEX_RADIUS * 3 / 4)
            let z = hex.x % 2 == 0 ? hex.z * (Settings.HEX_RADIUS * Math.sqrt(3) / 2) : hex.z * (Settings.HEX_RADIUS * Math.sqrt(3) / 2) + (Settings.HEX_RADIUS * Math.sqrt(3) / 4)
            x -= (maxX - minX) * Settings.HEX_RADIUS * 3 / 8
            z -= (maxZ - minZ + 0.5) * Settings.HEX_RADIUS * Math.sqrt(3) / 4

            let hexObj = new Hex3D(x, z, hex)
            switch (hex.type) {
                case "wall":
                    break
                case "enemy":
                    let ally = new Ally()
                    ally.getContainer().position.set(x + x, 0, z + z)
                    Game.allyObjects.push(ally)
                    Game.availableAllies.add(ally.getContainer())
                    break
                case "treasure":
                    let item = new Item(x, z)
                    this.items.push(item)
                    hexObj.getContainer().add(item.getContainer())
                    break
                case "light":
                    let light = new Light(x, z)
                    this.lights.push(light)
                    hexObj.getContainer().add(light.getContainer())
                    break
            }
            this.map.push(hexObj)
        }
    }

    getSize() {
        return this.size
    }

    getMap() {
        return this.map
    }
}

var LevelLoader = {
    loadFromServer: async function (name) {
        let resp = await $.ajax({
            type: "POST",
            url: "LOAD",
            data: JSON.stringify({ name: name })
        })

        return new Level(resp.size, resp.level)
    }
}