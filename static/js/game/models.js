var Models = {
    load: async function (model, texture) {
        let material = new THREE.MeshBasicMaterial({
            map: TextureLoader.load(texture),
            morphTargets: true
        })

        let geometry = await new Promise((resolve, reject) => {
            ModelLoader.load(model, geom => resolve(geom))
        })

        return new THREE.Mesh(geometry, material)
    },
}