class Hex3D {
    constructor(x, z, data) {
        this.x = x
        this.z = z
        this.data = data
        this.radius = Settings.HEX_RADIUS
        this.container = new THREE.Object3D()
        this.container.position.set(x, Settings.WALL_HEIGHT / 4, z)

        this.create()
    }

    create() {
        let wallGeom = new THREE.BoxGeometry(Settings.WALL_LENGTH(), Settings.WALL_HEIGHT, Settings.WALL_WIDTH)
        let wall = new THREE.Mesh(wallGeom, Materials.Wall)
        wall.position.y = this.container.position.y
        let floorGeom = new THREE.CylinderGeometry(Settings.HEX_RADIUS, Settings.HEX_RADIUS, 0, 6)
        let floor = new THREE.Mesh(floorGeom, Materials.Floor)
        floor.position.set(this.container.position.x, this.container.position.y - Settings.WALL_HEIGHT / 2, this.container.position.z)
        floor.rotation.set(0, Math.PI / 2, 0)
        floor.name = "FLOOR"
        this.container.add(floor)
        this.floor = floor
        this.walls = []

        for (let i = 0; i < 6; i++) {
            if (this.data.inDirs.includes(i) && this.data.dirOut != i) { continue }
            let side = wall.clone()

            let angle = Math.PI / 3 * (i + 4.5) //+4.5 żeby obrócić hex dopasowując go do edytora
            side.position.x = this.x + (Settings.HEX_RADIUS * Math.sqrt(3) / 2) * Math.cos(angle)
            side.position.z = this.z + (Settings.HEX_RADIUS * Math.sqrt(3) / 2) * Math.sin(angle)

            if (this.data.dirOut == i) {
                side.scale.set(1 / 3, 1, 1)

                let rAngle = -(Math.PI / 3 * i)
                side.rotation.set(0, rAngle, 0)

                let dist = Settings.WALL_LENGTH() / 3
                //let tgA = Math.tan(-rAngle)
                let dX = dist * Math.cos(-rAngle)//Math.sqrt(dist * dist / (tgA * tgA + 1))
                let dY = dist * Math.sin(-rAngle)//tgA * dX

                let otherSegment = side.clone()
                side.position.x += dX
                side.position.z += dY
                otherSegment.position.x -= dX
                otherSegment.position.z -= dY

                this.container.add(otherSegment)
                this.walls.push(otherSegment)
            } else
                side.lookAt(this.container.position)

            this.container.add(side)
            this.walls.push(side)
        }
    }

    getContainer() {
        return this.container
    }
}