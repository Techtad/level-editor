class Ally extends Entity {
    constructor() {
        super()
        this.speed = Settings.ALLY_SPEED

        //this.container.remove(this.axes)

        this.container.remove(this.model)
        Models.load("models/ally_model.js", "img/ally_model.png").then(mesh => {
            this.model = mesh
            this.model.scale.set(1.5, 1.5, 1.5)
            let size = new THREE.Vector3()
            new THREE.Box3().setFromObject(this.model).getSize(size)
            this.model.position.y += size.y / 2
            this.model.rotation.y += Math.PI / 2
            this.container.add(this.model)
            this.animationMixer = new THREE.AnimationMixer(this.model)
        })
        this.container.allyObj = this
    }

    moveTo(vector) {
        super.moveTo(vector)
        this.model.rotation.y += Math.PI / 2
        this.animationMixer.clipAction("Stand").stop()
        this.animationMixer.clipAction("run").play()
    }

    update(delta) {
        super.update(delta)

        if (this.animationMixer) {
            if (!this.moving) {
                this.animationMixer.clipAction("Stand").play()
                this.animationMixer.clipAction("run").stop()
            }
            this.animationMixer.update(delta / 56)
        }

        if (this.ring) this.ring.rotateZ(-delta / 100)
    }

    highlight() {
        this.ring = new AllyHighlight(35)
        this.container.add(this.ring)
        this.ring.rotation.set(Math.PI / 2, 0, 0)
        this.ring.position.set(0, 1, 0)
    }

    unHighlight() {
        if (this.ring) this.container.remove(this.ring)
        delete this.ring
    }
}

class AllyHighlight extends THREE.Mesh {
    constructor(radius) {
        let geom = new THREE.RingGeometry(radius, radius * 1.25, 6, 1)
        let mat = new THREE.MeshBasicMaterial({ color: 0x00ff00, side: THREE.DoubleSide })
        super(geom, mat)
    }
}