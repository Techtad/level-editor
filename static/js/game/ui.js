class UI {
    constructor() {
        this.addLightControls()
    }

    addLightControls() {
        $("#lightIntensity").on("input", function (event) {
            Settings.LIGHT_INTENSITY = parseFloat($(this).val())
            for (let l of Game.level.lights)
                l.updateParams()
        })
        $("#lightHeight").on("input", function (event) {
            Settings.LIGHT_HEIGHT = parseFloat($(this).val())
            for (let l of Game.level.lights)
                l.updateParams()
        })
    }
}