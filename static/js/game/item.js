class Item {
    constructor(x, z) {
        this.container = new THREE.Object3D()
        let boxGeom = new THREE.BoxGeometry(Settings.TREASURE_SIZE, Settings.TREASURE_SIZE, Settings.TREASURE_SIZE)
        this.box = new THREE.Mesh(boxGeom, Materials.Treasure)
        this.container.position.set(x, Settings.TREASURE_SIZE / 4, z)
        this.container.add(this.box)
    }

    getContainer() {
        return this.container
    }

    getBox() {
        return this.box
    }
}