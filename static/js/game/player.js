var PlayerControls = {
    mouseLeftDown: false,
    mouseRightDown: false,
    mouseX: 0,
    mouseY: 0
}

$(document).on("contextmenu", function (event) { event.preventDefault() })

$(document).on("mousedown", function (event) {
    if (event.button == 0 || event.button == 2) {
        if (event.button == 0) {
            PlayerControls.mouseLeftDown = true
            if (Game.highlightedAlly) {
                let dist = Game.player.getContainer().position.distanceTo(Game.highlightedAlly.getContainer().position)
                if (dist < 150) {
                    Game.player.allies.push(Game.highlightedAlly)
                    Game.availableAllies.remove(Game.highlightedAlly.getContainer())
                    Game.scene.add(Game.highlightedAlly.getContainer())
                    Game.highlightedAlly.unHighlight()
                    Game.highlightedAlly = null

                    if (Game.availableAllies.children.length == 0) Game.scene.remove(Game.availableAllies)
                }
            }
        }
        else if (event.button == 2) PlayerControls.mouseRightDown = true

        PlayerControls.mouseX = (event.clientX / $(window).width()) * 2 - 1
        PlayerControls.mouseY = -(event.clientY / $(window).height()) * 2 + 1
    }
})

$(document).on("mouseup", function (event) {
    if (event.button == 0) PlayerControls.mouseLeftDown = false
    else if (event.button == 2) PlayerControls.mouseRightDown = false
})

$(document).on("mousemove", function (event) {
    PlayerControls.mouseX = (event.clientX / $(window).width()) * 2 - 1
    PlayerControls.mouseY = -(event.clientY / $(window).height()) * 2 + 1

    if (Game.availableAllies && Game.availableAllies.children.length > 0) {
        let raycaster = new THREE.Raycaster()
        let mouseVector = new THREE.Vector2(PlayerControls.mouseX, PlayerControls.mouseY)
        raycaster.setFromCamera(mouseVector, Game.camera)
        let intersects = raycaster.intersectObjects(Game.availableAllies.children, true)
        if (intersects.length > 0) {
            let ally = intersects[0].object.parent.allyObj
            let id = intersects[0].object.parent.id
            if (!Game.highlightedAlly || Game.highlightedAlly.getContainer().id != id) {
                if (Game.highlightedAlly) Game.highlightedAlly.unHighlight()
                Game.highlightedAlly = ally
                Game.highlightedAlly.highlight()
            }
        } else if (Game.highlightedAlly) { Game.highlightedAlly.unHighlight(); Game.highlightedAlly = null }
    }
})

class Player extends Entity {
    constructor() {
        super()
        this.speed = Settings.PLAYER_SPEED
        this.collIndicator = null

        //this.container.remove(this.axes)

        this.container.remove(this.model)
        Models.load("models/player_model.js", "img/player_model.png").then(mesh => {
            this.model = mesh
            this.model.scale.set(1.75, 1.75, 1.75)
            let size = new THREE.Vector3()
            new THREE.Box3().setFromObject(this.model).getSize(size)
            this.model.position.y += size.y / 4 * 3
            this.model.rotation.y += Math.PI / 2
            this.container.add(this.model)
            this.animationMixer = new THREE.AnimationMixer(this.model)
        })

        this.allies = []
    }

    moveTo(vector) {
        super.moveTo(vector)
        this.model.rotation.y += Math.PI / 2
        this.animationMixer.clipAction("1stand").stop()
        this.animationMixer.clipAction("2run").play()
    }

    update(delta) {
        if (PlayerControls.mouseRightDown) {
            let raycaster = new THREE.Raycaster()
            let mouseVector = new THREE.Vector2(PlayerControls.mouseX, PlayerControls.mouseY)
            raycaster.setFromCamera(mouseVector, Game.camera)
            let intersects = raycaster.intersectObjects(Game.floors, true)
            if (intersects.length > 0 && intersects[0].object.name == 'FLOOR') {
                this.moveTo(intersects[0].point)
            }

            if (this.collIndicator) {
                raycaster = new THREE.Raycaster()
                let dir = new THREE.Vector3()
                this.axes.getWorldDirection(dir)
                let size = new THREE.Vector3()
                new THREE.Box3().setFromObject(this.model).getSize(size)
                let ray = new THREE.Ray(this.container.position.clone().add(new THREE.Vector3(0, size.y / 2, 0)), dir)
                raycaster.ray = ray

                intersects = raycaster.intersectObjects(Game.walls, true);
                if (intersects[0]) {
                    let pnt = intersects[0].point
                    this.collIndicator.position.set(pnt.x, pnt.y, pnt.z)
                    let distance = this.container.position.distanceTo(this.destination)
                    if (Math.abs(intersects[0].distance) <= Math.abs(distance)) {
                        let vec = this.direction.clone()
                        vec.multiplyScalar(35.0)
                        pnt.sub(vec)
                        this.moveTo(new THREE.Vector3(pnt.x, this.destination.y, pnt.z))
                    }
                }
            }
        }

        /* if (this.collIndicator) {
            let raycaster = new THREE.Raycaster()
            let dir = new THREE.Vector3()
            this.axes.getWorldDirection(dir)
            let ray = new THREE.Ray(this.container.position.clone().add(new THREE.Vector3(0, new THREE.Box3().setFromObject(this.model).getSize().y / 2, 0)), dir)
            raycaster.ray = ray

            var intersects = raycaster.intersectObjects(Game.walls, true);
            if (intersects[0]) {
                this.collIndicator.position.set(intersects[0].point.x, intersects[0].point.y, intersects[0].point.z)
                if (intersects[0].distance <= new THREE.Box3().setFromObject(this.model).getSize().z / 2) {
                    this.moving = false
                }
            }
        } */

        super.update(delta)

        if (this.animationMixer) {
            if (!this.moving) {
                this.animationMixer.clipAction("1stand").play()
                this.animationMixer.clipAction("2run").stop()
            }
            this.animationMixer.update(delta / 56)
        }

        for (let i = 0; i < this.allies.length; i++) {
            let ally = this.allies[i]
            let dist = ally.getContainer().position.distanceTo(this.getContainer().position)
            if (dist > (i + 1) * Settings.ALLY_DISTANCE)
                ally.moveTo(this.getContainer().position)
            else
                ally.moving = false
        }
    }
}