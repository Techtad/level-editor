class Light {
    constructor(x, z) {
        this.container = new THREE.Object3D()
        this.container.position.set(x, Settings.LIGHT_HEIGHT, z)
        this.light = new THREE.PointLight(0xffffff, Settings.LIGHT_INTENSITY, Settings.LIGHT_DISTANCE)
        this.container.add(this.light)

        let geom = new THREE.SphereGeometry(25, 8, 2)
        this.mesh = new THREE.Mesh(geom, Materials.LightIndicator)
        this.container.add(this.mesh)
        //this.obj.position.y = Settings.LIGHT_HEIGHT * 2
    }

    getContainer() {
        return this.container
    }

    getLight() {
        return this.light
    }

    updateParams() {
        this.light.intensity = Settings.LIGHT_INTENSITY
        this.light.distance = Settings.LIGHT_DISTANCE
        this.container.position.y = Settings.LIGHT_HEIGHT
    }
}