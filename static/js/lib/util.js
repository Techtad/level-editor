var HexGridUtils = {
    neighborCoords: function (x, y, dir) {
        let nY
        switch (dir) {
            case 0:
                return { x: x, y: y - 1 }
            case 1:
                nY = x % 2 == 0 ? y - 1 : y
                return { x: x + 1, y: nY }
            case 2:
                nY = x % 2 == 0 ? y : y + 1
                return { x: x + 1, y: nY }
            case 3:
                return { x: x, y: y + 1 }
            case 4:
                nY = x % 2 == 0 ? y : y + 1
                return { x: x - 1, y: nY }
            case 5:
                nY = x % 2 == 0 ? y - 1 : y
                return { x: x - 1, y: nY }
        }
    }
}

var PageUtils = {
    getURLParam: function (name) {
        let pairs = window.location.search.substr(1).split('&')
        let urlParams = []
        for (let pair of pairs) {
            let values = pair.split("=")
            urlParams[values[0]] = values[1]
        }

        return urlParams[name]
    }
}