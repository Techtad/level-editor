var LevelEditor = {
    HEX_WIDTH: 110,
    HEX_HEIGHT: 100,
    size: 0,
    selectedType: "wall",

    createWorkspace: function (size) {
        this.size = parseInt(size)
        $("#workspace").empty()
        for (let x = 0; x < size; x++) {
            for (let y = 0; y < size; y++) {
                let hex = $("<div>")
                hex.addClass("hexagon")

                let xPos = x * (this.HEX_WIDTH * 0.79)
                let yPos = x % 2 == 0 ? y * (this.HEX_HEIGHT * 1.04) : (y + 0.51) * (this.HEX_HEIGHT * 1.04)
                hex.css("left", `${xPos}px`)
                hex.css("top", `${yPos}px`)

                hex.attr("id", `hex-${x}-${y}`)

                hex.on("click", function (event) {
                    if (this.children().length == 0) {
                        this.attr("direction", 0)
                        this.attr("type", LevelEditor.selectedType)
                        this.attr("set", true)

                        let arrow = $("<div>")
                        arrow.addClass("arrow")
                        arrow.text(this.attr("direction"))
                        this.append(arrow)
                    } else {
                        let newDir = (parseInt(this.attr("direction")) + 1) % 6
                        this.attr("direction", newDir)
                        this.attr("type", LevelEditor.selectedType)
                        let arrow = this.children().first()
                        arrow.text(newDir)
                        arrow.css("transform", `rotate(${newDir * 60}deg)`)
                    }
                    LevelEditor.displayOutput()
                }.bind(hex))
                hex.on("contextmenu", function (event) {
                    event.preventDefault()
                    this.empty()
                    this.removeAttr("set")
                    this.removeAttr("direction")
                    this.removeAttr("type")
                    LevelEditor.displayOutput()
                }.bind(hex))

                $("#workspace").append(hex)
            }
        }
    },

    compileLevel: function () {
        let levelObj = {
            size: this.size,
            level: []
        }

        let id = 0
        for (let x = 0; x < this.size; x++) {
            for (let y = 0; y < this.size; y++) {
                let hex = $(`#hex-${x}-${y}`)
                if (hex.attr("set")) {
                    let dir = parseInt(hex.attr("direction"))
                    let type = hex.attr("type")
                    let inDirs = []
                    for (let i = 0; i < 6; i++) {
                        let nCoords = HexGridUtils.neighborCoords(x, y, i)
                        let nHex = $(`#hex-${nCoords.x}-${nCoords.y}`)
                        if (nHex.length && parseInt(nHex.attr("direction")) == (i + 3) % 6)
                            inDirs.push(i)
                    }
                    levelObj.level.push({ id: id, x: x, z: y, dirOut: dir, inDirs: inDirs, type: type })
                    id++
                }
            }
        }

        return levelObj
    },

    saveLevel: function (name) {
        obj = {
            name: name,
            data: this.compileLevel()
        }
        console.log(obj)

        $.ajax({
            url: "SAVE",
            data: JSON.stringify(obj),
            type: "POST",
            success: function () {
                console.log("Zapisano na serwerze")
            },
            error: function (xhr, status, error) {
                console.log(xhr, status, error);
            }
        })
    },

    displayOutput: function () {
        $("#output").text(JSON.stringify(this.compileLevel(), null, 4))
    },

    listLevels: function () {
        $.ajax({
            url: "LIST",
            data: null,
            type: "POST",
            success: function (response, state, req) {
                if (response) {
                    console.log("lista leveli", response)
                    $("#list-overlay").css("display", "block")
                    $("#levels").empty()

                    if (response.length > 0) {
                        for (let name of response) {
                            let btn = $("<div>")
                            btn.addClass("type-button")
                            btn.css("width", "250px")
                            btn.text(name)
                            btn.on("click", function (event) {
                                $("#list-overlay").css("display", "none")
                                $("#levels").empty()

                                LevelEditor.loadLevel($(this).text())
                                $("#lvlName").val($(this).text())
                            })
                            $("#levels").append(btn)
                        }
                    }
                }
            }.bind(this),
            error: function (xhr, status, error) {
                console.log(xhr, status, error);
            }
        })
    },

    loadLevel: function (name) {
        $.ajax({
            url: "LOAD",
            data: JSON.stringify({ name: name }),
            type: "POST",
            success: function (response, state, req) {
                if (response) {
                    this.size = response.size
                    $("#levelSize").val(this.size)
                    this.createWorkspace(this.size)

                    for (let hex of response.level) {
                        let elem = $(`#hex-${hex.x}-${hex.z}`)
                        elem.click()
                        elem.attr("set", true)
                        elem.attr("direction", hex.dirOut)
                        elem.attr("type", hex.type)
                        let arrow = elem.children().first()
                        arrow.text(hex.dirOut)
                        arrow.css("transform", `rotate(${hex.dirOut * 60}deg)`)
                    }

                    this.displayOutput()

                    console.log("Wczytano level: " + name)
                }
            }.bind(this),
            error: function (xhr, status, error) {
                console.log(xhr, status, error);
            }
        })
    }
}

$(document).ready(function (event) {
    $("#levelSize").on("input", function (event) {
        LevelEditor.createWorkspace($("#levelSize").val())
        LevelEditor.displayOutput()
    })
    $("#btnSave").on("click", () => {
        LevelEditor.saveLevel(encodeURI($("#lvlName").val()))
    })
    $("#btnLoad").on("click", LevelEditor.listLevels.bind(LevelEditor))
    $("#btnPlay").on("click", () => { window.location = `/game?lvl=${encodeURI($("#lvlName").val())}` })
    $(".type-button").each(function () {
        $(this).on("click", function (event) {
            LevelEditor.selectedType = this.attr("type")
            $(".type-button[selected]").removeAttr("selected")
            this.attr("selected", "")
        }.bind($(this)))
    })

    $("#btnMoveTest").on("click", () => { window.location = `/test.html?t=movement` })

    LevelEditor.createWorkspace($("#levelSize").val())
})