var mouseDown, mouseX, mouseY

var Game = {
    init: function () {
        this.scene = new THREE.Scene()

        this.renderer = new THREE.WebGLRenderer({ antialias: true })
        this.renderer.setClearColor(0xffffff)
        this.renderer.setSize($(window).width(), $(window).height())

        this.camera = new THREE.PerspectiveCamera(45, $(window).width() / $(window).height(), 1, 10000)
        this.camera.position.set(0, 300, 0)
        this.camera.lookAt(this.scene.position)
        this.camera.updateProjectionMatrix()

        window.addEventListener("resize", (e) => { Game.renderer.setSize($(window).width(), $(window).height()); Game.camera.aspect = $(window).width() / $(window).height(); Game.camera.updateProjectionMatrix(); })

        //this.orbitControls = new THREE.OrbitControls(this.camera, this.renderer.domElement);

        this.clock = new THREE.Clock()

        $("#root").append(this.renderer.domElement)
        this.createScene()
    },

    createScene: function () {
        this.sun = new THREE.DirectionalLight(0xffffff, 0.25)
        this.scene.add(this.sun)

        let basePlateGeom = new THREE.PlaneGeometry(1000, 1000, 25, 25)
        let floor = new THREE.Mesh(basePlateGeom, Materials.Floor)
        let grid = new THREE.Mesh(basePlateGeom, Materials.Grid)
        grid.rotation.set(Math.PI / 2 * 3, 0, 0)
        floor.rotation.set(Math.PI / 2 * 3, 0, 0)
        floor.position.z -= 1
        floor.name = 'FLOOR'
        this.scene.add(floor)
        this.floors = [floor]
        this.scene.add(grid)

        this.walls = []
        LevelLoader.loadFromServer("test").then((level) => {
            this.level = level
            for (let hex of level.getMap()) {
                this.scene.add(hex.getContainer())
                this.floors.push(hex.floor)
                this.walls = this.walls.concat(hex.walls)
            }
        })

        this.moveIndicator = new THREE.Mesh(new THREE.SphereGeometry(10, 16, 16), Materials.GridRed)
        this.collIndicator = new THREE.Mesh(new THREE.SphereGeometry(10, 16, 16), Materials.GridGreen)
        this.scene.add(this.moveIndicator)
        this.scene.add(this.collIndicator)

        this.player = new Player()
        this.player.collIndicator = this.collIndicator
        this.scene.add(this.player.getContainer())

        this.allyObjects = []
        this.availableAllies = new THREE.Object3D()
        this.scene.add(this.availableAllies)
        this.highlightedAlly = null

        let ally = new Ally()
        this.allyObjects.push(ally)
        ally.getContainer().position.x += 100
        this.availableAllies.add(ally.getContainer())

        let ally2 = new Ally()
        ally2.getContainer().position.x -= 100
        this.allyObjects.push(ally2)
        this.availableAllies.add(ally2.getContainer())
    },

    update: function () {
        let delta = this.clock.getDelta() / 0.016
        this.player.update(delta)
        for (let ally of this.allyObjects) { ally.update(delta) }

        this.camera.position.x = this.player.getContainer().position.x
        this.camera.position.z = this.player.getContainer().position.z + 400
        this.camera.position.y = this.player.getContainer().position.y + 400
        this.camera.lookAt(this.player.getContainer().position)

        this.moveIndicator.position.set(this.player.destination.x, 12.5, this.player.destination.z)

        this.render()
    },

    render: function () {
        this.renderer.render(this.scene, this.camera)
        requestAnimationFrame(this.update.bind(this))
    },


    start: function () {
        requestAnimationFrame(this.update.bind(this))
    }
}

$(document).ready(function (event) {
    Game.init()
    Game.start()
})