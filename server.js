var http = require("http")
var fs = require("fs")

function sendFile(fileName, res) {
    let directory = "static"
    if (fileName == "/") fileName = "/editor.html"
    if (fileName == "/game") fileName = "/game.html"
    if (fileName.includes("/libs")) directory = __dirname
    fs.readFile(`${directory}${fileName}`, function (error, data) {
        if (error) {
            console.log(error)
            res.writeHead(404, { "Content-Type": "text/html;charset=utf-8" })
            res.end(`Błąd 404: Nie znaleziono pliku ./static${fileName}`)
        } else {
            let ext = fileName.split(".")[1]
            let contentType = "text/html;charset=utf-8"
            switch (ext) {
                case "js":
                    contentType = "application/javascript"
                    break
                case "css":
                    contentType = "text/css"
                    break
                case "jpg":
                    contentType = "image/jpeg"
                    break
                case "mp3":
                    contentType = "audio/mpeg"
                    break
                case "png":
                    contentType = "image/png"
                    break
                case "svg":
                    contentType = "image/svg+xml"
                    break
                default: break
            }
            res.writeHead(200, { "Content-Type": contentType })
            res.end(data)
        }
    })
}

function saveLevel(level, name) {
    if (!fs.existsSync("levels")) fs.mkdirSync("levels")
    fs.writeFileSync(`levels/${name}.json`, JSON.stringify(level))
}

function loadLevel(name) {
    return JSON.parse(fs.readFileSync(`levels/${name}.json`))
}

function servResponse(req, res) {
    let allData = ""
    req.on("data", function (data) {
        allData += data
    })
    req.on("end", function (data) {
        console.log("Data(" + req.url + "):" + allData)
        let obj
        if (allData) {
            try {
                obj = JSON.parse(allData)
            } catch (error) {
                console.log(error)
                res.writeHead(400, { "Content-Type": "text/html;charset=utf-8" })
                res.end(error)
            }
        }
        switch (req.url) {
            case "/SAVE":
                console.log("SAVE_REQ", JSON.stringify(obj, null, 4))
                if (obj && obj.data && obj.name) {
                    saveLevel(obj.data, obj.name)
                    res.writeHead(200, { "Content-Type": "text/html;charset=utf-8" })
                    res.end("OK")
                } else {
                    res.writeHead(400, { "Content-Type": "text/html;charset=utf-8" })
                    res.end("Błędne żądanie")
                }
                break
            case "/LIST":
                try {
                    let levelList = []
                    if (!fs.existsSync("levels")) fs.mkdirSync("levels")
                    fs.readdirSync("levels").forEach((file) => {
                        levelList.push(file.split(".")[0])
                    })
                    res.writeHead(200, { "Content-Type": "application/json" })
                    res.end(JSON.stringify(levelList))
                } catch (error) {
                    console.log(error)
                    res.writeHead(400, { "Content-Type": "text/html;charset=utf-8" })
                    res.end(error)
                }
                break
            case "/LOAD":
                console.log("LOAD_REQ", obj)
                if (obj && obj.name) {
                    let which = obj.name
                    if (fs.existsSync(`levels/${which}.json`)) {
                        let level = loadLevel(which)
                        res.writeHead(200, { "Content-Type": "application/json" })
                        res.end(JSON.stringify(level))
                    } else {
                        res.writeHead(400, { "Content-Type": "text/html;charset=utf-8" })
                        res.end("Błąd ładowania levela")
                    }
                } else {
                    res.writeHead(400, { "Content-Type": "text/html;charset=utf-8" })
                    res.end("Błędne żądanie")
                }
                break
            default:
                console.log("Błędny POST: " + req.url)
                res.writeHead(400, { "Content-Type": "application/json;charset=utf-8" })
                res.end("Błędna akcja POST: " + req.url)
                break
        }
    })
}

var server = http.createServer(function (req, res) {
    console.log(req.method + " : " + req.url)
    switch (req.method) {
        case "GET":
            sendFile(decodeURI(req.url).split('?')[0], res)
            break;
        case "POST":
            servResponse(req, res)
            break;
    }
})

const port = 3000
server.listen(3000, function () {
    console.log("Start serwera na porcie " + port)
})